import java.util.*;
public class LuckyCardGameApp{
	public static void main(String[] args){
		// plays the LuckyCardGame
		Scanner reader = new Scanner(System.in);
		playGame();
	}
	
	public static void playGame(){
		Scanner reader = new Scanner(System.in);
		GameManager manager = new GameManager();
		// New game manager created
		int totalPoints = 0; //to count the player's points
		int roundNumber = 1; // to count the round number and tell the user their round number
		System.out.println("Hello there, Lets play the lucky card game.");
		while(manager.getNumberOfCards() > 0 && totalPoints < 5){
			System.out.println("Round: " + roundNumber);
			System.out.println(manager); // tells the player their card and centre card
			totalPoints += manager.calculatePoints(); // calculates the points for the player and informs them how many points they are at.
			System.out.println("Your current score is " + totalPoints + "\n\n******************************************************************\n");
			manager.dealCards();
			roundNumber++;
		}
		// Tells the player their final score and prints a statement depending on whether they win or loose.
		System.out.println("Your final score was " + totalPoints);
		if(totalPoints >= 5){
			System.out.println("Great Job, you got lucky. You should get a lottery ticket.\nYou won with 5 points");
		}
		else{
			System.out.println("Sorry, you got unlucky and lost the game. I would stay away from any form of betting for a while.");
		}
	}
	public static void drawCards(Deck deckOfCards){
		//Asks the user how many cards they want to draw from the deck of cards
		Scanner reader = new Scanner(System.in);
		boolean invalid = true; // used to confirm whether the number the player entered is valid or not
		System.out.print("Hello There. How many cards would you like to draw from the deck: ");
		int numberOfDraws = Integer.parseInt(reader.nextLine());
		while(invalid){ // checks whether the player entered a valid number of cards and asks the player for a valid number of cards to draw.
			if(numberOfDraws < deckOfCards.length() && numberOfDraws > 0){
				invalid = false;
				for(int i = 0; i < numberOfDraws; i++){
					deckOfCards.drawTopCard();
				}
			}
			else{
				System.out.println("Please enter another amount of cards to draw. There are only " + deckOfCards.length() + " left");
				numberOfDraws = Integer.parseInt(reader.nextLine());
			}
		}
	}
}