public class Card{
	//Two fields for a card: suits and values
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		//initializing the values of the field using the parameters
		this.suit = suit;
		this.value = value;
	}
	public String getSuit(){
		// returns the suit of the card
		return this.suit;
	}
	public String getValue(){
		// returns the value of the card
		return this.value;
	}
	public String toString(){
		//changes the output of printing a card.
		return this.value + " of " + this.suit;
	}
}