import java.util.*;
public class Deck{
	//Three fields for a deck: the cards, number of cards and a random to shuffle the deck
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		//Initializes the cards to the cards of a normal deck, creates the random for later use and initializes the number of cards.
		this.rng = new Random();
		this.cards = new Card[52];
		this.numberOfCards = 52;
		//Two string arrays to create a nested loop to make it easier to create all card types.
		String[] suits = new String[] {"Hearts", "Diamonds", "Clubs", "Spades"};
		String[] values = new String[] {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		int cardPosition = 0;
		for(int i = 0; i < suits.length; i++){
			for(int j = 0; j < values.length; j++){
				this.cards[cardPosition] = new Card(suits[i], values[j]);
				cardPosition++;
			}
		}
	}
	public Card[] getCards(){
		//returns the cards in the deck.
		return this.cards;
	}
	public int length(){
		//returns how many cards are left in the deck.
		return this.numberOfCards;
	}
	public Card drawTopCard(){
		//draws the top card and reduces the number of cards in the deck by 1.
		this.numberOfCards--;
		return this.cards[this.numberOfCards];
	}
	public String toString(){
		//changes how a deck is printed to the terminal.
		String cards = "";
		for(int i = 0; i < this.cards.length; i++){
			cards+= this.cards[i] + "\n";
		}
		return cards;
	}
	public void shuffle(){
		//shuffles the deck by swapping a card with a random card in the remaining cards in the deck
		Card placeholder = this.cards[0];
		for (int i = 0; i < this.numberOfCards; i++){
			int randomNumber = this.rng.nextInt(this.numberOfCards);
			placeholder = this.cards[i];
			cards[i] = cards[randomNumber];
			cards[randomNumber] = placeholder;
		}
	}
}