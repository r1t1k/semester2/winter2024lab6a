public class GameManager{
	//Three fields for the gameManager: the deck from which the cards are drawn, a centre card and a player card which is drawn from the deck.
	private Deck drawPile;
	private Card centreCard;
	private Card playerCard;
	
	public GameManager(){
		//initialized the deck and shuffles the deck, initializes the centre and player card by drawing the top 2 cards, 1 for the centre and 1 for the player.
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centreCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	public String toString(){
		//changes how a Game Manager type is printed to the terminal.
		return "--------------------------------\nCentre card: " + this.centreCard + "\nPlayer card: " + this.playerCard + "\n--------------------------------";
    }
	public void dealCards(){
		//shuffles the deck and draws 2 new cards, 1 for the centre and 1 for the player.
		this.drawPile.shuffle();
		this.centreCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	public int getNumberOfCards(){
		// returns the number of cards left in the deck.
		return this.drawPile.length();
	}
	public int calculatePoints(){
		//calculates points by comparing the player and centre card. If the suits are same gives 2 points to the use, if the values are the same gives 4 points to the player and if none are same, takes away 1 point from the player.
		if(this.playerCard.getSuit().equals(this.centreCard.getSuit())){
			return 2;
		}
		if(this.playerCard.getValue().equals(this.centreCard.getValue())){
			return 4;
		}
		else{
			return -1;
		}
	}
	public Deck getDrawPile(){
		// returns the deck of cards.
		return this.drawPile;
	}
}